package main;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import config.Dbconfig;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DeleteDocument {

    private static Dbconfig dbconfig= new Dbconfig();
    static ArrayList<String> uuid = new ArrayList<String>();
    static List<String> ListauuidNo = new ArrayList<String>();

    public static void main(String[] args) throws IOException {

        MongoDatabase dbO = dbconfig.ConectionDB();


        List<String> identifiers = new ArrayList<String>();

//        FileReader fileData= new FileReader("C:/Users/Windows/Desktop/reporteMongo/reporte.txt");
//        BufferedReader bf = new BufferedReader(fileData);
//        String sCadena="";
//        while ((sCadena = bf.readLine())!=null) {
//            identifiers.add(sCadena);
//            System.out.println(sCadena);
//        }

        // CODEAN ESTA CLASE PARA ELIMINAR

        // AGREGUE UN ARRAYLIST PARA IDS
        // se eliminaron los empleados con los IDS enviados de oigual manera se eliminan en mongo Atlas
        // ESO SERIA  HASTA AHORA VOY A SUBIRLO A MI REPOSITORIO PARA QUE LO PUEDAN DESCARGAR Y MAPEAR EL CODIGO
        // PARA SUBIR DE INTELLIJ A GITLAB HACEMOS LO SIGUIENTE:
        //VCS -> Enable version control
        // Luego VSC - git -> remotes
        // tal cual lo estoy haciendo
        // ahora hay que comitear el projecto presionas  Control + K
        // una ves comiteado nos falta hacer un Push
        // presionas control + shift + K
        //Colocan sus credenciales



        identifiers.add("P4");
        identifiers.add("P5");
        identifiers.add("P6");
        identifiers.add("P7");

        // con un for hacemos la busqueda de cada identifier
        // procedemos a eliminar
        for(int i=0;i<identifiers.size();i++){
            work(identifiers.get(i),dbO);
        }

    }

    public static void work(String identifier, MongoDatabase dbO) {
        System.out.println("---CONEXION A MongoAtlas EXITOSA---");

        String fin = findOne(identifier, dbO);
        if (!fin.equals("")) {
            uuid.add(fin);
        } else {
            System.out.println("---El Identifier no existe---");
            ListauuidNo.add(identifier);
        }
    }

    public static String findOne(String identifier, MongoDatabase dbO) {

        String respuesta = "";
        boolean resp = DeleteDocument(identifier, dbO);
        if (resp) {
            respuesta = identifier;
            System.out.println("Documento eliminado " + identifier);
        } else {
            System.out.println("ERROR AL ELIMINAR " + identifier);
        }

        return respuesta;
    }

    public static boolean DeleteDocument(String identifier, MongoDatabase db) {
        Bson filter = new Document("identifier", identifier);
        return delete(filter, "empleado", db);
    }


    public static boolean delete(Bson filter, String collection, MongoDatabase db) {
        MongoCollection<Document> c = db.getCollection(collection);
        DeleteResult result = c.deleteOne(filter);
        Boolean response = false;
        if (result.getDeletedCount() != 0) {
            response = true;
        }
        return response;
    }
}
