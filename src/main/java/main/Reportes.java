package main;


import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import com.mongodb.client.model.Filters;
import config.Dbconfig;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class Reportes {
    static List<String> lista = new ArrayList<String>();
    private static Dbconfig dbconfig = new Dbconfig();
    public static void main(String[] args) {

       MongoDatabase dbO= dbconfig.ConectionDB();
        // control + espacio importas
        Document document=null;
        Bson filter=null;

        //  Bson extension de formato json para nuestra consulta
        Bson fapellido = new Document("apellido","Suyo");

        // realizamos el filtro correspondiente con Filters.and y la extesion json
        filter= Filters.and(fapellido);
        MongoCursor<Document> result = dbO.getCollection("empleado").find(filter).iterator();


        boolean exist=false;

        while (result.hasNext()) {

             document= result.next();
            System.out.println(document.toJson().toString());

            // pero yo quiero generarlo como reporte en txt ????
            String nombre = document.getString("nombre");


            // quiero el nombre del empleado + la fecha de ingreso
            //LLAMA FECHA EMISION
            Date date=document.getDate("IssueDate");
//				SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
            SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd");
            sdfPeru.setTimeZone(TimeZone.getTimeZone("America/Peru/Lima"));
            String fecha = sdfPeru.format(date);

            // creamos una arreglo de lisat de tipo String se va guardar todos los nombre
            lista.add("nombre:" + nombre +" Fecha de registro: "+fecha);

            //   USTEDES PUEDEN CREAR VARIOS D ETIPO DE REPORTES SOLO EXPLOTEN SU POTENCIAL
            // VOY A GREAGR UNA CLASE PAR ELIMINAR SOLO CODEEN EL CODIGO

        }

        // va crear un archivo txt con el nombre asignado reporte.txt en la ruta C:/Users/Windows/Desktop/reporteMongo
        // y agregara toda la inforamcion del arreglo lista

        // Luego crearemos un fichero que cree un txt y guarde todo el arreglo lista
        File archivos = new File("C:/Users/Windows/Desktop/reporteMongo/reportexD.txt");
        FileWriter fichero1=null;
        PrintWriter pw1=null;
        try {
            if(archivos.createNewFile()){
                System.out.println("TXT  creado");
            }
            fichero1=new FileWriter(archivos);
            pw1= new PrintWriter(fichero1);
            for(int it = 0; it < lista.size(); it++){
                pw1.println(lista.get(it));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try{
                if(null != fichero1)
                    fichero1.close();
            }catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }
}
